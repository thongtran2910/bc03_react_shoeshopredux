import logo from "./logo.svg";
import "./App.css";
import BTShoeShopRedux from "./BTShoeShopRedux/BTShoeShopRedux";

function App() {
  return (
    <div className="App">
      <BTShoeShopRedux />
    </div>
  );
}

export default App;
