import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART } from "./redux/constants/constant";

class ProductItem extends Component {
  render() {
    let { image, name, price } = this.props.data;
    return (
      <div className="card col-3">
        <img src={image} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{price}</p>
          <a
            onClick={() => {
              this.props.handleThemSp(this.props.data);
            }}
            className="btn btn-success"
          >
            Add to cart
          </a>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleThemSp: (sanPham) => {
      dispatch({
        type: ADD_TO_CART,
        payload: sanPham,
      });
    },
  };
};

export default connect(null, mapDispatchToProps)(ProductItem);
