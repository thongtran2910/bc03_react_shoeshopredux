import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoeShop } from "./dataShoeShop";
import ProductItem from "./ProductItem";
import ProductList from "./ProductList";

export default class BTShoeShopRedux extends Component {
  state = {
    gioHang: [],
  };
  render() {
    return (
      <div>
        <ProductList />
        <h4>
          Số lượng sản phẩm trong giỏ hàng: {this.state.gioHang.length}
          {""}
        </h4>
        <Cart />
      </div>
    );
  }
}
