import { dataShoeShop } from "../../dataShoeShop";
import { ADD_TO_CART, TANG_GIAM_SL, XOA_SAN_PHAM } from "../constants/constant";

let initialState = {
  productList: dataShoeShop,
  gioHang: [],
};

export const shoeShopReducer = (
  state = initialState,
  { type, payload, value }
) => {
  switch (type) {
    case ADD_TO_CART: {
      let cloneGioHang = [...state.gioHang];
      let index = cloneGioHang.findIndex((item) => {
        return item.id == payload.id;
      });
      if (index == -1) {
        let newSanPham = { ...payload, soLuong: 1 };
        cloneGioHang.push(newSanPham);
      } else {
        cloneGioHang[index].soLuong++;
      }
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    case XOA_SAN_PHAM: {
      let cloneGioHang = [...state.gioHang];
      let index = cloneGioHang.findIndex((item) => {
        return item.id == payload;
      });
      if (index !== -1) {
        cloneGioHang.splice(index, 1);
        state.gioHang = cloneGioHang;
        return { ...state };
      }
    }
    case TANG_GIAM_SL: {
      let cloneGioHang = [...state.gioHang];
      let index = cloneGioHang.findIndex((item) => {
        return item.id == payload;
      });
      if (index !== -1) {
        cloneGioHang[index].soLuong += value;
      }
      cloneGioHang[index].soLuong == 0 && cloneGioHang.splice(index, 1);
      state.gioHang = cloneGioHang;
      return { ...state };
    }
    default:
      return state;
  }
};
